package com.pankaj.rest.client;

import com.pankaj.rest.models.Comment;
import com.pankaj.rest.models.Message;

import javax.ws.rs.client.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.HashMap;
import java.util.Map;

public class RestApiClient {
    public static void main(String[] args) {
        Client client = ClientBuilder.newClient();

        WebTarget baseTarget = client.target("http://localhost:8080/webapi/");
        WebTarget messageTarget = baseTarget.path("messages");
        WebTarget singleMessageTarget = messageTarget.path("{messageId}");

       /* Response response = client.target("http://localhost:8080/webapi/messages/1")
                .request()
                .get();
        System.out.println(response);
        System.out.println(response.readEntity(Message.class).getMessage());*/

      /*  WebTarget webTarget = client.target("http://localhost:8080/webapi/messages/2");
        Invocation.Builder builder = webTarget.request(MediaType.APPLICATION_JSON);
        Message response = builder.get(Message.class);
        System.out.println(response.getMessage());
      */

        /*Message message = client.target("http://localhost:8080/webapi/messages/2")
                .request(MediaType.APPLICATION_JSON)
                .get(Message.class);
        System.out.println(message.getMessage());*/

     /*   String message = client.target("http://localhost:8080/webapi/messages/2")
                .request(MediaType.APPLICATION_JSON)
                .get(String.class);
        System.out.println(message);*/

        Message message1 = singleMessageTarget
                .resolveTemplate("messageId", "1")
                .request(MediaType.APPLICATION_JSON)
                .get(Message.class);
        System.out.println(message1.getMessage());

        Message message2 = singleMessageTarget
                .resolveTemplate("messageId", "2")
                .request(MediaType.APPLICATION_JSON)
                .get(Message.class);
        System.out.println(message2.getMessage());

        Comment newComment = new Comment(1, "First Comment to POST","Pankaj");
        Map commentMap = new HashMap();
        commentMap.put(1, newComment);
        Message newMessage = new Message(3, "Learn about POST", "Pankaj3", commentMap);

        Response postResponse = messageTarget
                .request()
                .post(Entity.json(newMessage));
        System.out.println(postResponse);
        if(postResponse.getStatus() != 201){
            System.out.println("Error Posting");
        }
        Message createMessage = postResponse.readEntity(Message.class);
        System.out.println(createMessage.getMessage());

    }
}
