package com.pankaj.rest.database;

import com.pankaj.rest.models.Comment;
import com.pankaj.rest.models.Message;
import com.pankaj.rest.models.Profile;

import java.util.HashMap;
import java.util.Map;

public class TempStorage {

    private static Map<Long, Message> messageMap = new HashMap<>();
    private static Map<String, Profile> profileMap = new HashMap<>();
   private static Map<Long, Comment> commentMap = new HashMap<>();

    public static Map<Long, Message> getMessageMap(){
        return messageMap;
    }

    public static Map<String, Profile> getProfileMap(){
        return profileMap;
    }

    public static Map<Long, Comment> getCommentMap(){
        return commentMap;
    }


}
