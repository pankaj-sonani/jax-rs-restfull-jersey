package com.pankaj.rest.resources;

import com.pankaj.rest.models.Comment;
import com.pankaj.rest.services.CommentService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/")
@Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
public class CommentResource {

    private CommentService commentService = new CommentService();

    @GET
    public List<Comment> getAllComments(@PathParam("messageId") long messageId){
        return commentService.getAllComments(messageId);
    }
    @GET
    @Path("/{commentId}")
    public Comment getCommentsById(@PathParam("messageId") long messageid, @PathParam("commentId") long commentId){
        return commentService.getCommentById(messageid, commentId);
    }

    @POST
    public Comment addComment(@PathParam("messageId") long messageId, Comment comment){
        return commentService.addComment(messageId, comment);
    }

    @PUT
    @Path("/{commentId}")
    public Comment modifyComment(@PathParam("messageId") long messageId, @PathParam("commentId") long commentId, Comment comment){
        comment.setId(commentId);
        return commentService.modifyComment(messageId, comment);
    }

    @DELETE
    @Path("/{commentId}")
    public void deleteComment(@PathParam("messageId") long messageId, @PathParam("commentId") long commentId){
        commentService.deleteComment(messageId, commentId);
    }
}
