package com.pankaj.rest.resources;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

@Path("/injectdemo")
@Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN})
@Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN})
public class InjectDemoResource {

    @GET
    @Path("anno")
    public String getParamUsingAnnotatins(@MatrixParam("param") String matrixParam, @HeaderParam("customHeader") String customHeader, @CookieParam("name") String cookie){
        return "This is Matrix Param " + matrixParam + "\nthis customer header values " + customHeader + "\nthis is cookie value " +cookie;
    }

    @GET
    @Path("context")
    public String getParamsUsingContext(@Context UriInfo uriInfo, @Context HttpHeaders httpHeaders){
        String absolutePath = uriInfo.getAbsolutePath().toString();
        String cookies = httpHeaders.getCookies().toString();
        return absolutePath + "\nI have cookei too" + cookies;
    }
}
