package com.pankaj.rest.resources;

import com.pankaj.rest.models.Message;
import com.pankaj.rest.models.Profile;
import com.pankaj.rest.resources.beans.MassageFilterBean;
import com.pankaj.rest.services.MessageService;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

@Path("/messages")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class MessageResource {

    MessageService messageService = new MessageService();

   /* @GET
    public List<Message> getMessages(@QueryParam("year") int year, @QueryParam("start") int start, @QueryParam("size") int size) {
        if(year > 0){
            return messageService.getAllMessagesForYear(year);
        }
        if(start >= 1 && size >= 1){
            return messageService.getAllMessagesPaginated(start,size);
        }
        return messageService.getAllMessages();
    }*/
   @GET
   @Produces(value = {MediaType.APPLICATION_JSON, MediaType.TEXT_XML})
   public List<Message> getJsonMessages(@BeanParam MassageFilterBean messageFilterBean, @Context UriInfo uriInfo) {
       System.out.printf("Json Method called");
       if (messageFilterBean.getYear() > 0) {
           return messageService.getAllMessagesForYear(messageFilterBean.getYear());
       }
       if (messageFilterBean.getStart() >= 1 && messageFilterBean.getSize() >= 1) {
           return messageService.getAllMessagesPaginated(messageFilterBean.getStart(), messageFilterBean.getSize());
       }
       return messageService.getAllMessages();
   }

    @GET
    @Path("/{messageId}")
    public Message getMessageById(@PathParam("messageId") long messageId, @Context UriInfo uriInfo){
        Message message = messageService.getMessage(messageId);
        message.addLink(getUrlForSelf(uriInfo, message),"self");
        message.addLink(getUrlForProfile(uriInfo, message),"Profile");
        message.addLink(getUrlForComments(uriInfo, message),"comments");
        return message;
    }


    @POST
    public Response addMessage(Message message, @Context UriInfo uriInfo) throws URISyntaxException {
        Message newMessage = messageService.addMessage(message);
        String newId = String.valueOf(newMessage.getId());
        URI locationUri = uriInfo.getAbsolutePathBuilder().path(newId).build();
        return Response.created(locationUri)
                .entity(newMessage)
                .build();
    }

    @PUT
    @Path("/{messageId}")
    public Message modifyMessage(@PathParam("messageId") long messageId, Message message){
        message.setId(messageId);
        return messageService.modifyMessage(message);
    }

    @DELETE
    @Path("/{messageId}")
    public Message deleteMessage(@PathParam("messageId") long messageId){
        return messageService.deleteMessage(messageId);
    }

   /* @GET
    @Path("/{messageId}/comments")
    public String getComments(){
        return "test comments";
    }*/
    @Path("/{messageId}/comments")
    public CommentResource getCommentResource(){
        return new CommentResource();
    }

    private String getUrlForSelf(@Context UriInfo uriInfo, Message message) {
        return uriInfo.getBaseUriBuilder()
                .path(MessageResource.class)
                .path(String.valueOf(message.getId()))
                .build()
                .toString();
    }
    private String getUrlForProfile(@Context UriInfo uriInfo, Message message) {
        return uriInfo.getBaseUriBuilder()
                .path(ProfileResource.class)
                .path(message.getAuthor())
                .build()
                .toString();
    }
    private String getUrlForComments(UriInfo uriInfo, Message message) {
        return uriInfo.getBaseUriBuilder()
                .path(MessageResource.class)
                .path(MessageResource.class, "getCommentResource")
                .path(CommentResource.class)
                .resolveTemplate("messageId", message.getId())
                .build()
                .toString();
    }


}
