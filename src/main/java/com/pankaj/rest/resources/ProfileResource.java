package com.pankaj.rest.resources;

import com.pankaj.rest.models.Message;
import com.pankaj.rest.models.Profile;
import com.pankaj.rest.services.MessageService;
import com.pankaj.rest.services.ProfileService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/profiles")
@Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
public class ProfileResource {

    ProfileService profileService = new ProfileService();

    @GET
    public List<Profile> getProfiles() {
        return profileService.getAllProfiles();
    }

    @GET
    @Path("/{profileName}")
    public Profile getMessageById(@PathParam("profileName") String profileName){
        return profileService.getProfile(profileName);
    }

    @POST
    public Profile addMessage(Profile profile){
        return profileService.addProfile(profile);
    }

    @PUT
    @Path("/{profileName}")
    public Profile modifyMessage(@PathParam("profileName") String profileName, Profile profile){
        profile.setProfileName(profileName);
        return profileService.modifyProfile(profile);
    }

    @DELETE
    @Path("/{profileName}")
    public Profile deleteMessage(@PathParam("profileName") String profileName){
        return profileService.deleteProfile(profileName);
    }
}
