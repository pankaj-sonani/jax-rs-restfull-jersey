package com.pankaj.rest.services;

import com.pankaj.rest.database.TempStorage;
import com.pankaj.rest.models.Comment;
import com.pankaj.rest.models.ErrorMessage;
import com.pankaj.rest.models.Message;

import javax.ws.rs.NotFoundException;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class CommentService {
    private Map<Long, Message> messages =  TempStorage.getMessageMap();
    private Map<Long, Comment> comments = null;//TempStorage.getCommentMap();

    public List<Comment> getAllComments(long messageId){
        comments = messages.get(messageId).getComments();
        return new ArrayList<Comment>(comments.values());
    }

    public Comment getCommentById(long messageId, long commentId){
        ErrorMessage errorMessage = new ErrorMessage("Error Message", 404, "Message Id is not found in TempDBMap");
        Response response = Response.status(Response.Status.NOT_FOUND)
                .entity(errorMessage)
                .build();
        Message message = messages.get(messageId);
        if(message == null){
            throw new WebApplicationException(response);
        }
        comments = messages.get(messageId).getComments();
        Comment comment = comments.get(commentId);
        if(comment == null){
            throw new NotFoundException(response);
        }
        return comment;
    }

    public Comment addComment(long messageId, Comment comment){
        comments = messages.get(messageId).getComments();
        comment.setId(comments.size()+1);
        comments.put(comment.getId(), comment);
        return comment;
    }

    public Comment modifyComment(long messageId, Comment comment){
        comments = messages.get(messageId).getComments();
        if(comment.getId() <= 0){
            return null;
        }
        comments.put(comment.getId(), comment);
        return comment;
    }

    public Comment deleteComment(long messageId, long commentId){
        comments = messages.get(messageId).getComments();
        return comments.remove(commentId);
    }
}
