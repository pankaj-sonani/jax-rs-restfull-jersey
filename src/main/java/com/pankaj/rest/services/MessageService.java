package com.pankaj.rest.services;

import com.pankaj.rest.database.TempStorage;
import com.pankaj.rest.exceptions.DataNotFoundException;
import com.pankaj.rest.models.Comment;
import com.pankaj.rest.models.Message;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

public class MessageService {

    private Map<Long, Message> messageMap = TempStorage.getMessageMap();
    private Map<Long, Comment> commentMap = TempStorage.getCommentMap();

    public MessageService() {
        messageMap.put(1L, new Message(1, "Message One - About Java from default constructor", "Pankaj", commentMap));
        messageMap.put(2L, new Message(2, "Message Two - About Jersey from default constructor", "Payal", commentMap));
    }

    public List<Message> getAllMessages(){

        return new ArrayList<Message>(messageMap.values());
    }

    public List<Message> getAllMessagesForYear(int year){
        List<Message> messageForYearList = new ArrayList<>();
        Calendar cal = Calendar.getInstance();
        for(Message message : messageMap.values()){
            cal.setTime(message.getCreated());
            if(cal.get(Calendar.YEAR) == year){
                messageForYearList.add(message);
            }
        }
        return messageForYearList;
    }

    public List<Message> getAllMessagesPaginated(int start, int size){
        List<Message> messageList = new ArrayList<Message>(messageMap.values());
        if(start + size > messageList.size()) return new ArrayList<>();
        return messageList.subList(start, start+size);
    }

    public Message getMessage(long id){
        Message message = messageMap.get(id);
        if(message == null){
            throw new DataNotFoundException("Message with id "+id+ "Not Found");
        }
        return message;
    }

    public Message addMessage(Message message){
        message.setId(messageMap.size()+1);
        messageMap.put(message.getId(), message);
        return message;
    }
    public Message modifyMessage(Message message){
        if(message.getId() <= 0){
            return null;
        }else {
            messageMap.put(message.getId(), message);
            return message;
        }
    }
    public Message deleteMessage(long id){
        return messageMap.remove(id);
    }
}
