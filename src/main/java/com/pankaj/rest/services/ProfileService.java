package com.pankaj.rest.services;

import com.pankaj.rest.database.TempStorage;
import com.pankaj.rest.models.Profile;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ProfileService {

    private Map<String, Profile> profileMap = TempStorage.getProfileMap();

    public ProfileService() {
        profileMap.put("Pankaj", new Profile(1L, "Pankaj-Profile", "Pankaj", "Sonani"));
    }

    public List<Profile> getAllProfiles(){
       return new ArrayList<Profile>(profileMap.values());
    }

    public Profile getProfile(String profileName){
        return profileMap.get(profileName);
    }

    public Profile addProfile(Profile profile){
        profile.setId(profileMap.size()+1);
        profileMap.put(profile.getProfileName(), profile);
        return profile;
    }
    public Profile modifyProfile(Profile profile){
        if(profile.getProfileName().isEmpty()){
            return null;
        }else {
            profileMap.put(profile.getProfileName(), profile);
            return profile;
        }
    }
    public Profile deleteProfile(String profileName){
        return profileMap.remove(profileName);
    }
}
